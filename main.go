package main

import (
	"fmt"
	"log"

	"angga/github/mystartup/auth"
	"angga/github/mystartup/config"
	"angga/github/mystartup/handler"
	"angga/github/mystartup/services/users/app"
	"angga/github/mystartup/user"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func main() {

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		config.HOST, config.PORT, config.USERNAME, config.PASSWORD, config.DBNAME)
	db, err := gorm.Open(postgres.Open(psqlInfo), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}

	userRepository := user.NewRepository(db)
	userService := user.NewService(userRepository)
	authService := auth.NewService()

	userHandler := handler.NewUserHandler(userService, authService)

	router := gin.Default()
	api := router.Group("/api/v1")

	api.POST("/users/createuser", userHandler.RegisterUser)
	api.POST("/users/login", userHandler.LoginByEmail)
	api.POST("/check/email", userHandler.CheckEmailAvailability)
	api.POST("/upload/avatar", userHandler.UploadAvatar)
	router.Run(":8080")
}

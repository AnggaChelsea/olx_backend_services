package auth

import (
	"angga/github/mystartup/config"

	"github.com/dgrijalva/jwt-go"
)

type Service interface {
	Generat6eToken(userID int) (string, error)
}

type jwtService struct {

}

func NewService() *jwtService {
	return &jwtService{}
}

func (s *jwtService) Generat6eToken(userID int) (string, error) {
	claim := jwt.MapClaims{}
	claim["user_id"] = userID
	token := jwt.NewWithClaims(jwt.SigningMethodES256, claim)
	tokenString, err := token.SignedString(config.SECRET_KEY)
	if err != nil {
		return tokenString, err
	}
	return tokenString, nil
}

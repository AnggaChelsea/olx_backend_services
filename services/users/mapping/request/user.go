package request

import "angga/github/mystartup/services/users/domain/entity"

type UserFormatter struct {
	ID    int    `json:"id"`
	Name  string `json:"name"`
	Email string `json:"email"`
	Image string `json:"image"`
	Phone string `json:"phone"`
	Token string `json:"token"`
}

func FormatUser(user entity.User) UserFormatter {
	forrmatter := UserFormatter{
		ID:    user.ID,
		Name:  user.Name,
		Email: user.Email,
		Image: user.Image,
		Phone: user.Phone,
	}
	return forrmatter
}

package response

type Response struct {
	Meta Meta        `json:"meta"`
	Data interface{} `json:"data"`
}

type Meta struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
	Status  string `json:"status"`
}

// ada dua choose bisa pake ini
// func APIResponse(message string, code int, status string, data interface{}) (*response, error) {
// 	return &response{
// 		mete: meta{
// 			message: message,
// 			code:    code,
// 			status:  status,
// 		},
// 		data: data,
// 	}, nil
// }

//bisa juga pake yang ini

func APIResponse(message string, code int, status string, data interface{}) Response {
	meta := Meta{
		Message: message,
		Code:    code,
		Status:  status,
	}
	reponses := Response{
		Meta: meta,
		Data: data,
	}
	return reponses
}

package entity

import "time"

type User struct {
	ID             int
	Name           string
	Email          string
	Image          string
	Password       string
	AvatarFileName string
	RoleId         int
	Phone          string
	UpdatedAt      time.Time
	CreatedAt      time.Time
}

type RegistrerUserInput struct {
	Name           string
	Email          string
	Image          string
	Password       string
	Role           string
	Phone          string
	AvatarFileName string
	CreatedAt      time.Time
	UpdatedAt      time.Time
}

type LoginByEmail struct {
	Email    string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type CheckEmailAvailability struct {
	Email string `json:"email" binding:"required"`
}

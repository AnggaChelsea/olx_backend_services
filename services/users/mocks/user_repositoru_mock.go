package mocks

import (
	"angga/github/mystartup/services/users/domain/entity"

	"github.com/stretchr/testify/mock"
)

type UserRepositoryMock struct {
	Mock mock.Mock
}

func (repository *UserRepositoryMock) SaveAvatar(ID int, file string) *entity.User {
	arguments := repository.Mock.Called(ID, file)
	if arguments.Get(0) == nil {
		return nil
	} else {
		user := arguments.Get(0).(entity.User)
		return &user
	}

}

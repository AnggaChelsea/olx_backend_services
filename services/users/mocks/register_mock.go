package mocks

import "github.com/stretchr/testify/mock"

type RegisterRepositoryMock struct {
	repositoryMock mock.Mock
}
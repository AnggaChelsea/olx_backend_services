package repository

import (
	"angga/github/mystartup/services/users/domain/entity"

	"gorm.io/gorm"
)

type Repository interface {
	Save(user entity.User) (entity.User, error)
	FindByEmail(user entity.User) (entity.User, error)
	FindById(user entity.User) (entity.User, error)
	UpdateUser(user entity.User) (entity.User, error)
}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}
//masih maslaah disini jadi meskipun error masih ke save ke daabase
func (r *repository) Save(user entity.User) (entity.User, error) {
	err := r.db.Create(&user).Error
	if err != nil {
		return user, nil
	}
	return user, nil
}

func (r *repository) FindByEmail(user entity.User) (entity.User, error) {
	err := r.db.Where("email = ?", user.Email).Find(&user).Error
	if err != nil {
		return user, err
	}
	return user, nil
}

func (r *repository) FindById(user entity.User) (entity.User, error) {
	err := r.db.Where("ID = ?", user.ID).Find(&user).Error
	if err != nil {
		return user, err
	}
	return user, nil
}

func (r *repository) UpdateUser(user entity.User) (entity.User, error) {
	err := r.db.Save(&user).Error
	if err != nil {
		return user, err
	}
	return user, nil
}

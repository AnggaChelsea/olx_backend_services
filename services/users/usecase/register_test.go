package usecase

import (
	"angga/github/mystartup/services/users/repository"
	"testing"
	"time"
)

func TestRegister(t *testing.T) {
	type repouser struct {
		repository repository.Repository
	}
	users := []struct {
		name             string
		email            string
		image            string
		phone            string
		avatar_file_name string
		password         string
		role_id          int
		created_at       time.Time
		updated_at       time.Time
	}{
		{
			name:             "Angga",
			email:            "angga@gmail.com",
			image:            "file.jpg",
			phone:            "123456",
			avatar_file_name: "file.jpg",
			password:         "password",
			role_id:          1,
			created_at:       time.Now(),
			updated_at:       time.Now(),
		},
	}
	for _, test := range users {
		t.Run(test.name, func(t *testing.T) {
			

		})
	}

}

package usecase

import (
	"angga/github/mystartup/services/users/domain/entity"
)

func (s *service) IsEmailAvaliable(input entity.CheckEmailAvailability) (bool, error) {
	user, err := s.repository.FindByEmail(entity.User{
		Email: input.Email,
	})
	if err != nil {
		return false, err
	}
	if user.ID != 0 {
		return false, nil
	}
	return true, nil
}

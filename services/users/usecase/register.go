package usecase

import (
	"angga/github/mystartup/services/users/domain/entity"
	"time"

	"golang.org/x/crypto/bcrypt"
)

func (s *service) RegisterUser(input entity.RegistrerUserInput) (entity.User, error) {
	bs, _ := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.MinCost)

	user, err := s.repository.Save(entity.User{
		Name:           input.Name,
		Email:          input.Email,
		Image:          input.Image,
		Phone:          input.Phone,
		AvatarFileName: input.AvatarFileName,
		Password:       string(bs),
		RoleId:         1,
		CreatedAt:      time.Now(),
		UpdatedAt:      time.Now(),
	})

	if err != nil {
		return user, err
	}
	return user, nil
}

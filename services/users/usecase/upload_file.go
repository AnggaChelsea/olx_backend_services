package usecase

import "angga/github/mystartup/services/users/domain/entity"

func (s *service) SaveAvatar(ID int, fileLocation string) (entity.User, error) {
	user, err := s.repository.FindById(entity.User{
		ID: ID,
	})
	if err != nil {
		return user, err
	}
	user.Image = fileLocation
	user.AvatarFileName = fileLocation
	updateUser, err := s.repository.UpdateUser(user)
	if err != nil {
		return updateUser, err
	}
	return updateUser, nil
}

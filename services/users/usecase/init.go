package usecase

import (
	"angga/github/mystartup/services/users/domain/entity"
	"angga/github/mystartup/services/users/repository"
)

type Service interface {
	RegisterUser(input entity.RegistrerUserInput) (entity.User, error)
	LoginByEmail(input entity.LoginByEmail) (entity.User, error)
	IsEmailAvaliable(input entity.CheckEmailAvailability) (bool, error)
	SaveAvatar(ID int, fileLocation string) (entity.User, error)
}
type service struct {
	repository repository.Repository
}

func NewService(repository repository.Repository) *service {
	return &service{
		repository: repository,
	}
}

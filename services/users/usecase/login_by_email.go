package usecase

import (
	"angga/github/mystartup/services/users/domain/entity"
	"errors"

	"golang.org/x/crypto/bcrypt"
)

func (s *service) LoginByEmail(input entity.LoginByEmail) (entity.User, error) {

	user, err := s.repository.FindByEmail(entity.User{
		Email:    input.Email,
		Password: input.Password,
	})
	if err != nil {
		return user, err
	}
	if user.ID == 0 {
		return user, errors.New("User notfound")
	}
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(input.Password))
	if err != nil {
		return user, err
	}
	return user, nil
}

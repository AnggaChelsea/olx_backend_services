package app

import (
	"angga/github/mystartup/services/users/config"
	"angga/github/mystartup/services/users/handler"
	"angga/github/mystartup/services/users/repository"
	"angga/github/mystartup/services/users/usecase"
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func Application() {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		config.HOST, config.PORT, config.USERNAME, config.PASSWORD, config.DBNAME)

	db, err := gorm.Open(postgres.Open(psqlInfo), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}

	userRepository := repository.NewRepository(db)
	userService := usecase.NewService(userRepository)
	userHandler := handler.NewUserHandler(userService)

	router := gin.Default()
	api := router.Group("/api/v1")

	api.POST("/user/register", userHandler.RegisterUser)
	api.POST("/user/login", userHandler.LoginByEmail)
	router.Run(":8000")
}

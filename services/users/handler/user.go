package handler

import (
	"angga/github/mystartup/services/users/mapping/request"
	"angga/github/mystartup/services/users/mapping/response"
	"angga/github/mystartup/services/users/usecase"
	"angga/github/mystartup/services/users/domain/entity"
	"net/http"

	"github.com/gin-gonic/gin"
)

type userHandler struct {
	userService usecase.Service
}

func NewUserHandler(userService usecase.Service) *userHandler {
	return &userHandler{userService: userService}
}

func (h *userHandler) LoginByEmail(c *gin.Context){
	var inputLogin entity.LoginByEmail
	err := c.ShouldBindJSON(&inputLogin)
	if err != nil {
		responseserr := response.APIResponse("Maaf email password kurang tepat", http.StatusUnprocessableEntity, "err", err.Error())
		c.JSON(http.StatusBadRequest, responseserr)
	}
	loginNew, err := h.userService.LoginByEmail(inputLogin)
	format := request.FormatUser(loginNew)
	if err != nil {
		responseserr := response.APIResponse("Maaf email password kurang tepat", http.StatusUnprocessableEntity, "err", err.Error())
		c.JSON(http.StatusBadRequest, responseserr)
	}else{
		response := response.APIResponse("Login Success", http.StatusOK, "data", format)
		c.JSON(http.StatusOK, response)
		c.JSON(http.StatusOK, 200)
	}
}

func (h *userHandler) RegisterUser(c *gin.Context) {
	//tangkap iunput dari register inpunter
	var input entity.RegistrerUserInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		responseserr := response.APIResponse("Maaf ada yang error", http.StatusUnprocessableEntity, "err", err.Error())
		c.JSON(http.StatusBadRequest, responseserr)
	}

	newUser, err := h.userService.RegisterUser(input)
	forrmater := request.FormatUser(newUser)
	if err != nil {
		responses := response.APIResponse("Maaf ada yang error", http.StatusUnprocessableEntity, "err", err.Error())
		c.JSON(http.StatusBadRequest, responses)
	}
	response := response.APIResponse("Yey Selamat Account mu telah terdaftar", http.StatusOK, "data", forrmater)
		c.JSON(http.StatusOK, response)
		c.JSON(http.StatusOK, 200)
}

func (h *userHandler) CheckEmailAvailability(c *gin.Context) {

	var input entity.CheckEmailAvailability
	err := c.ShouldBindJSON(&input)
	if err != nil {
		responses := response.APIResponse("email checking failed", http.StatusUnprocessableEntity, "err", err.Error())
		c.JSON(http.StatusBadRequest, responses)
		return
	}
	isEmailAvaliable, err := h.userService.IsEmailAvaliable(input)
	if err != nil {
		errormessage := gin.H{"errors": "server error"}
		responses := response.APIResponse("email checking failed", http.StatusUnprocessableEntity, "err", errormessage)
		c.JSON(http.StatusBadRequest, responses)
		return
	}
	data := gin.H{
		"is_avaliable": isEmailAvaliable,
	}
	var metaMessage string
	if isEmailAvaliable {
		metaMessage = "email is not avaliable"
		responses := response.APIResponse(metaMessage, http.StatusNotFound, "err", data)
		c.JSON(http.StatusNotFound, responses)
	} else {

		metaMessage = "email is avaliable"
		responses := response.APIResponse(metaMessage, http.StatusOK, "success", data)
		c.JSON(http.StatusAccepted, responses)
	}
}

func (h *userHandler) UploadAvatar(c *gin.Context) {

	file, err := c.FormFile("avatar_file_name")
	if err != nil {
		data := gin.H{
			"is_uploaded": false,
		}
		responses := response.APIResponse("filed to uploadAvatar", http.StatusUnprocessableEntity, "err", data)
		c.JSON(http.StatusBadRequest, responses)
		return
	}
	path := "assets/images/" + file.Filename
	err = c.SaveUploadedFile(file, path)
	if err != nil {
		data := gin.H{
			"is_uploaded": true,
		}
		responses := response.APIResponse("success to uploadAvatar", http.StatusOK, "success", data)
		c.JSON(http.StatusBadRequest, responses)
		return
	}

	//harusnya dapat jwt
	userID := 4
	_, err = h.userService.SaveAvatar(userID, path)
	if err != nil {
		data := gin.H{
			"is_uploaded": false,
		}
		responses := response.APIResponse("filed to uploadAvatar", http.StatusUnprocessableEntity, "err", data)
		c.JSON(http.StatusBadRequest, responses)
		return
	}
	data := gin.H{
		"is_uploaded": true,
	}
	responses := response.APIResponse("success to uploadAvatar", http.StatusOK, "success", data)
	c.JSON(http.StatusOK, responses)

}

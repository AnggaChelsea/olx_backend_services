package usecase

import (
	"angga/github/mystartup/services/products/entity"
)

type InputProduct struct {
	Name        string
	Description string
	Price       string
	Stock       string
	Status      string
	Type        entity.TypeProduct
}

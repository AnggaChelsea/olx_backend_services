package entity

import (
	"angga/github/mystartup/services/users/entity"
)

type Product struct {
	Name        string
	Description string
	Price       string
	Stock       string
	Status      string
	Type        TypeProduct
	Penjual     []entity.User
}
